package br.ucsal.bes20201.ed.aula06reposicao;

public class Exemplo1 {

	public static void main(String[] args) {
		executarExemplo1();
	}

	private static void executarExemplo1() {

		// ListaAluno alunos = new ListaAlunoEncadeada();
		ListaAluno alunos = new ListaAlunoArray();

		alunos.add(new Aluno(123, "antonio"));
		alunos.add(new Aluno(436, "claudio"));
		alunos.add(new Aluno(789, "maria"));
		alunos.add(new Aluno(279, "ana"));

		System.out.println("Primeiro aluno na lista=" + alunos.get(0));
		System.out.println("Segundo aluno na lista (antes da remoção)=" + alunos.get(1));
		
		System.out.println("Tamanho da lista (antes de remoção)=" + alunos.size());
		
		System.out.println("Mandei remover o segundo elemento da lista...");
		alunos.remove(1);
		
		System.out.println("Segundo aluno na lista (após a remoção)=" + alunos.get(1));
		System.out.println("Tamanho da lista (após a remoção)=" + alunos.size());
		System.out.println("A lista está vazia?" + alunos.isEmpty());

		System.out.println("Mandei remover o primeiro elemento da lista...");
		alunos.remove(0);

		System.out.println("Primeiro aluno na lista (após a segunda remoção)=" + alunos.get(0));
		System.out.println("Segundo aluno na lista (após a segunda remoção)=" + alunos.get(1));
		
		System.out.println("Limpei a lista...");
		alunos.clear();
		
		System.out.println("Tamanho da lista=" + alunos.size());
		System.out.println("A lista está vazia?" + alunos.isEmpty());
		
	}
}
