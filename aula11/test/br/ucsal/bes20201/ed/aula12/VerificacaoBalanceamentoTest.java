package br.ucsal.bes20201.ed.aula12;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class VerificacaoBalanceamentoTest {

	@Test
	public void testarBalanceada() {
		String expressao = "( 3 + 4 ) * [ 3 + 5 * ( 4 + 2 ) ] * [ 3 * ( 2 + 1 ) + 9 ]";
		Assertions.assertTrue(VerificacaoBalanceamento.isExpressaoBalanceada(expressao));
	}

	@Test
	public void testarNaoBalanceada1() {
		String expressao = "( 3 + 4 ) * [ 3 + 5 * ( 4 + 2 ] ) * [ 3 * ( 2 + 1 ) + 9 ]";
		Assertions.assertFalse(VerificacaoBalanceamento.isExpressaoBalanceada(expressao));
	}

	@Test
	public void testarNaoBalanceada2() {
		String expressao = "( 3 + 4 ) * ( 3 + 5 * [ 4 + 2 ) ] * [ 3 * ( 2 + 1 ) + 9 ]";
		Assertions.assertFalse(VerificacaoBalanceamento.isExpressaoBalanceada(expressao));
	}

	@Test
	public void testarNaoBalanceada3() {
		String expressao = "( 3 + 4 ]";
		Assertions.assertFalse(VerificacaoBalanceamento.isExpressaoBalanceada(expressao));
	}

	@Test
	public void testarNaoBalanceada4() {
		String expressao = "( 3 + 4 ) * [ 3 + 5 * ( 4 + 2 )";
		Assertions.assertFalse(VerificacaoBalanceamento.isExpressaoBalanceada(expressao));
	}

}
