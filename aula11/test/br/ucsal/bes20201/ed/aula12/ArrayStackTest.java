package br.ucsal.bes20201.ed.aula12;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Implementação da pilha utilizando array como estrutura para armazenamento dos
 * dados da mesma.
 * 
 * @author claudioneiva
 *
 */
public class ArrayStackTest {

	private Stack<String> stack1;

	@BeforeEach
	public void setup() {
		stack1 = new ArrayStack<>();

		stack1.push("antonio");
		stack1.push("claudio");
		stack1.push("pedreira");
		stack1.push("neiva");
	}

	@Test
	public void testarPop1() {
		Assertions.assertEquals("neiva", stack1.pop());
		Assertions.assertEquals("pedreira", stack1.pop());
		Assertions.assertEquals("claudio", stack1.pop());
		Assertions.assertEquals("antonio", stack1.pop());
	}

	@Test
	public void testarTop1() {
		Assertions.assertEquals("neiva", stack1.top());
		Assertions.assertEquals("neiva", stack1.top());
	}

	@Test
	public void testarSize1() {
		Assertions.assertEquals(4, stack1.size());
	}

	@Test
	public void testarSize2() {
		stack1.pop();
		Assertions.assertEquals(3, stack1.size());
		stack1.pop();
		Assertions.assertEquals(2, stack1.size());
		stack1.pop();
		stack1.pop();
		Assertions.assertEquals(0, stack1.size());
	}

	@Test
	public void testarIsEmpty1() {
		Assertions.assertFalse(stack1.isEmpty());
	}

	@Test
	public void testarIsEmpty2() {
		Assertions.assertTrue(new ArrayStack<String>().isEmpty());
	}

	@Test
	public void testarIsEmpty3() {
		stack1.pop();
		stack1.pop();
		stack1.pop();
		stack1.pop();
		Assertions.assertTrue(stack1.isEmpty());
	}
}
