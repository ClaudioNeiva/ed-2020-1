package br.ucsal.bes20201.ed.aula12;

public class ArrayStack<T> implements Stack<T> {

	private static final int QTY_MAX = 100;

	private Object[] objects = new Object[QTY_MAX];

	private int top = 0;

	@Override
	public void push(T element) {
		// Verifica se a pilha está cheia.
		if (top == QTY_MAX) {
			throw new RuntimeException("Stack full.");
		}
		objects[top] = element;
		top++;
	}

	@SuppressWarnings("unchecked")
	@Override
	public T pop() {
		// Verifica se a pilha está vazia.
		if (top == 0) {
			throw new RuntimeException("Stack empty.");
		}
		top--;
		T element = (T) objects[top];
		objects[top] = null;
		return element;
	}

	@SuppressWarnings("unchecked")
	@Override
	public T top() {
		// Verifica se a pilha está vazia.
		if (top == 0) {
			throw new RuntimeException("Stack empty.");
		}
		return (T) objects[top - 1];
	}

	@Override
	public boolean isEmpty() {
		return top == 0;
	}

	@Override
	public int size() {
		return top;
	}

}
