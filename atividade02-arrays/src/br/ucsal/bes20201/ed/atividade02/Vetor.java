package br.ucsal.bes20201.ed.atividade02;

import java.util.Scanner;

public class Vetor {

	private Integer[] vet;
	
	private static Scanner sc = new Scanner(System.in);

	public Vetor(int qtd) {
		vet = new Integer[qtd];
	}

	public void obter(String mens) {
		System.out.println(mens);
		for (int i = 0; i < vet.length; i++) {
			vet[i] = sc.nextInt();
		}
	}

	public Vetor somar(Vetor vet2) {
		Vetor vetSoma = new Vetor(vet.length);
		for (int i = 0; i < vet.length; i++) {
			vetSoma.vet[i] = vet[i] + vet2.vet[i];
		}
		return vetSoma;
	}

	public void exibir(String mens) {
		System.out.println(mens);
		for (int i = 0; i < vet.length; i++) {
			System.out.println(vet[i]);
		}
	}

}
