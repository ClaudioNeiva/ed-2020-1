package br.ucsal.bes20201.ed.aula07;

public class Exemplo1 {
	
	public static void main(String[] args) {
		executarExemplo1();
	}

	private static void executarExemplo1() {
		ListaString nomes; // = new EncadeadaListaString();
		nomes.add("antonio");
		nomes.add("claudio");
		nomes.add("maria");
		nomes.add("ana");
		System.out.println("Primeiro nome na lista="+nomes.get(0));
		System.out.println("Segudno nome na lista (antes da remoção)="+nomes.get(1));
		System.out.println("Tamanho da lista (antes de remoção)="+nomes.size());
		nomes.remove(1);
		System.out.println("Segundo nome na lista (após a remoção)="+nomes.get(1));
		System.out.println("Tamanho da lista (após a remoção)="+nomes.size());
		System.out.println("A lista está vazia?"+nomes.isEmpty());
		System.out.println("Limpei a lista...");
		nomes.clear();
		System.out.println("Tamanho da lista="+nomes.size());
		System.out.println("A lista está vazia?"+nomes.isEmpty());
	}
}
