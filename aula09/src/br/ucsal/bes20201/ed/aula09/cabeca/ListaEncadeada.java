package br.ucsal.bes20201.ed.aula09.cabeca;

public class ListaEncadeada<T> implements Lista<T> {

	private No cabeca;

	private int qtd = 0;
	
	public ListaEncadeada() {
		cabeca = new No();
	}

	@Override
	public void add(T element) {
		No aux = cabeca;
		while (aux.prox != null) {
			aux = aux.prox;
		}
		No novo = new No(element);
		aux.prox = novo;
		qtd++;
	}

	@Override
	public T get(int index) {
		No aux = cabeca.prox;
		for (int i = 0; i < index; i++) {
			aux = aux.prox;
		}
		return aux.element;
	}

	@Override
	public T remove(int index) {
		No noAnterior = cabeca;
		No aux = cabeca.prox;
		for (int i = 0; i < index; i++) {
			noAnterior = aux;
			aux = aux.prox;
		}
		T elemento = aux.element;
		noAnterior.prox = aux.prox;
		qtd--;
		return elemento;
	}

	@Override
	public int size() {
		return qtd;
	}

	@Override
	public boolean isEmpty() {
		return qtd == 0;
	}

	@Override
	public void clear() {
		cabeca = new No();
		qtd = 0;
	}

	private class No {
		private T element;
		private No prox;

		private No() {
		}

		private No(T element) {
			this.element = element;
		}

	}

}
