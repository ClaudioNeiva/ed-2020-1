package br.ucsal.bes20201.ed.aula09.generics;

public class Exemplo1 {

	public static void main(String[] args) {
		
		// No java 6:
		//Lista<Integer> lista1 = new ListaEncadeada<Integer>();
		
		// Do java 7 em diante:
		Lista<Integer> lista1 = new ListaEncadeada<>();
		lista1.add(12);
		lista1.add(2);
		lista1.add(3);
		System.out.println(lista1.get(0));
		
		Lista<String> lista2 = new ListaEncadeada<>();
		lista2.add("antonio");
		lista2.add("claudio");
		lista2.add("neiva");
		System.out.println(lista2.get(0));
		
	}
	
}
