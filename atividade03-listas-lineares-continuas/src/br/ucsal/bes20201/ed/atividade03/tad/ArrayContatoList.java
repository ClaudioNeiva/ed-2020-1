package br.ucsal.bes20201.ed.atividade03.tad;

import br.ucsal.bes20201.ed.atividade03.domain.Contato;

public class ArrayContatoList implements ContatoList {

	private static final int QTD_MAX = 100;

	private static final int INICIO = 0;

	private Contato[] vet = new Contato[QTD_MAX];

	private int fim = INICIO;

	@Override
	public void add(Contato element) {
		vet[fim] = element;
		fim++;
	}

	/**
	 * Retornar a posição do contato que possui nome igual ao passado como
	 * parâmetro. Caso nenhum contato possua nome igual ao passado como parâmetro, o
	 * método retorna -1.
	 */
	@Override
	public int indexOf(String nome) {
		for (int i = 0; i < fim; i++) {
			if (vet[i].getNome().equals(nome)) {
				// Contato da posição i possui nome igual ao nome passado como parâmetro;
				return i;
			}
		}

		// Se não saiu no meio do laço é porque nenhum contato tem nome igual ao passado
		// como parâmetro, então o método deve retornar -1.
		return -1;
	}

	@Override
	public Contato get(int index) {
		return vet[index];
	}

	@Override
	public int size() {
		return fim;
	}

}
