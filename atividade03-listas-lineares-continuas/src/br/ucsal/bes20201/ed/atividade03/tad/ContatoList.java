package br.ucsal.bes20201.ed.atividade03.tad;

import br.ucsal.bes20201.ed.atividade03.domain.Contato;

public interface ContatoList {

	void add(Contato element);

	// TODO O ideal seria passar um Contato como parâmetro.
	int indexOf(String nome);

	Contato get(int index);

	int size();

}
