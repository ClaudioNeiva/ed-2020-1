package br.ucsal.bes20201.ed.arvorebusca;

import javax.swing.JPanel;

//FIXME Refatorar para remover a herança de JPanel.

/**
 * 
 * https://www.baeldung.com/java-avl-trees
 * 
 * @author claudioneiva
 *
 * @param <T>
 */
public abstract class SearchBinaryTree<T extends Comparable<T>> extends JPanel {

	private static final long serialVersionUID = 1L;

	public static final boolean BALANCE = true;

	protected Node<T> root;

	public void add(T element) {
		clearSelect(root);
		root = add(element, root);
	}

	public Node<T> search(T element) {
		clearSelect(root);
		return search(element, root);
	}

	public void remove(T element) {
		clearSelect(root);
		root = remove(element, root);
	}

	private Node<T> add(T element, Node<T> node) {
		if (node == null) {
			node = new Node<T>(null, element, null);
		} else {
			int compare = element.compareTo(node.element);
			if (compare < 0) {
				node.left = add(element, node.left);
			} else if (compare > 0) {
				node.right = add(element, node.right);
			}
		}
		updateHeight(node);
		if (BALANCE) {
			node = balance(node);
		}
		return node;
	}

	private Node<T> balance(Node<T> node) {
		int balanceFactor = node.getBalanceFactor();
		if (balanceFactor > 1) {
			if (height(node.right.right) > height(node.right.left)) {
				node = rotateLeft(node);
			} else {
				node.right = rotateRight(node.right);
				node = rotateLeft(node);
			}
		} else if (balanceFactor < -1) {
			if (height(node.left.left) > height(node.left.right))
				node = rotateRight(node);
			else {
				node.left = rotateLeft(node.left);
				node = rotateRight(node);
			}
		}
		return node;
	}

	private Node<T> rotateLeft(Node<T> node) {
		Node<T> aux1 = node.right;
		Node<T> aux2 = aux1.left;
		aux1.left = node;
		node.right = aux2;
		updateHeight(node);
		updateHeight(aux1);
		return aux1;
	}

	private Node<T> rotateRight(Node<T> node) {
		Node<T> aux1 = node.left;
		Node<T> aux2 = aux1.right;
		aux1.right = node;
		node.left = aux2;
		updateHeight(node);
		updateHeight(aux1);
		return aux1;
	}

	private void updateHeight(Node<T> node) {
		node.height = 1 + Math.max(height(node.left), height(node.right));
	}

	public int height(Node<T> node) {
		return node == null ? -1 : node.height;
	}

	private Node<T> search(T element, Node<T> node) {
		if (node == null) {
			return null;
		}
		visit(node);
		int compare = element.compareTo(node.element);
		if (compare < 0) {
			return search(element, node.left);
		} else if (compare > 0) {
			return search(element, node.right);
		}
		return node;
	}

	private Node<T> remove(T element, Node<T> node) {
		if (node != null) {
			int compare = element.compareTo(node.element);
			if (compare < 0) {
				node.left = remove(element, node.left);
			} else if (compare > 0) {
				node.right = remove(element, node.right);
			} else if (node.left != null && node.right != null) {
				findMoreLeftNode(node.right).left = node.left;
				node = node.right;
			} else {
				node = (node.left != null) ? node.left : node.right;
			}
			if (BALANCE && node != null) {
				node = balance(node);
			}
		}
		return node;
	}

	private Node<T> findMoreLeftNode(Node<T> node) {
		while (node.left != null) {
			node = node.left;
		}
		return node;
	}

	protected abstract void visit(Node<T> node);

	protected abstract void clearSelect(Node<T> node);

}
