package br.ucsal.bes20201.ed.arvorebusca;

import java.awt.Dimension;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.SwingWorker;

public class App implements ActionListener {

	private static final int WINDOW_WIDTH = 1270;

	private static final int WINDOW_HEIGHT = 550;

	private static final int BUTTON_WIDTH = 220;

	private static final int BUTTON_HEIGHT = 40;

	private static final int TEXT_FIELD_WIDTH = WINDOW_WIDTH / 10;

	private static final int TEXT_FIELD_HEIGHT = BUTTON_HEIGHT;

	private SearchBinaryTreePanel<Aluno> searchBinaryTreePanel;

	private JButton addButton;
	private JButton searchButton;
	private JButton removeButton;

	private TextField matriculaTextField;
	private TextField nomeTextField;
	private TextField emailTextField;

	private JFrame frame;

	public static void main(String[] args) throws InterruptedException {
		new App().exec();
	}

	private void exec() throws InterruptedException {
		searchBinaryTreePanel = createSearchBinaryTree();
		frame = createJFrame();
	}

	private SearchBinaryTreePanel<Aluno> createSearchBinaryTree() {
		SearchBinaryTreePanel<Aluno> searchBinaryTreePanel = new SearchBinaryTreePanel<>();
		searchBinaryTreePanel.add(new Aluno(10, "Clara", "clr@gmail.com"));
		searchBinaryTreePanel.add(new Aluno(20, "Claudio", "claudo@ucsal.br"));
		searchBinaryTreePanel.add(new Aluno(30, "Pedro", "pedrao@bol.com.br"));
		searchBinaryTreePanel.add(new Aluno(40, "Antonio", "antonio@gmail.com"));
		searchBinaryTreePanel.add(new Aluno(50, "Joaquim", "joa@uol.com.br"));
		searchBinaryTreePanel.add(new Aluno(60, "Maria", "maria@yahoo.com"));
		searchBinaryTreePanel.add(new Aluno(70, "Ana", "ana@gmail.com"));
		return searchBinaryTreePanel;
	}

	private JFrame createJFrame() {
		JFrame jFrame = new JFrame("Árvore Binária de Busca " + (SearchBinaryTree.BALANCE ? "- AVL" : ""));
		jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jFrame.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);

		JPanel searchPanel = createSearchPanel(jFrame);

		JSplitPane splitPane = new JSplitPane();
		splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		splitPane.setDividerLocation(BUTTON_HEIGHT + 10);

		splitPane.setTopComponent(searchPanel);
		splitPane.setBottomComponent(searchBinaryTreePanel);

		jFrame.add(splitPane);

		jFrame.setVisible(true);

		return jFrame;
	}

	private JPanel createSearchPanel(JFrame jFrame) {
		JPanel searchPanel = new JPanel();

		JLabel matriculaLabel = new JLabel("Matrícula:");
		matriculaTextField = new TextField();
		matriculaTextField.setPreferredSize(new Dimension(TEXT_FIELD_WIDTH, TEXT_FIELD_HEIGHT));

		JLabel nomeLabel = new JLabel("Nome:");
		nomeTextField = new TextField();
		nomeTextField.setPreferredSize(new Dimension(TEXT_FIELD_WIDTH, TEXT_FIELD_HEIGHT));

		JLabel emailLabel = new JLabel("Email:");
		emailTextField = new TextField();
		emailTextField.setPreferredSize(new Dimension(TEXT_FIELD_WIDTH, TEXT_FIELD_HEIGHT));

		addButton = new JButton("Adicionar");
		addButton.setPreferredSize(new Dimension(BUTTON_WIDTH, BUTTON_HEIGHT));
		addButton.addActionListener(this);

		searchButton = new JButton("Pesquisar por matrícula");
		searchButton.setPreferredSize(new Dimension(BUTTON_WIDTH, BUTTON_HEIGHT));
		searchButton.addActionListener(this);

		removeButton = new JButton("Remover por matrícula");
		removeButton.setPreferredSize(new Dimension(BUTTON_WIDTH, BUTTON_HEIGHT));
		removeButton.addActionListener(this);

		searchPanel.add(matriculaLabel);
		searchPanel.add(matriculaTextField);

		searchPanel.add(nomeLabel);
		searchPanel.add(nomeTextField);

		searchPanel.add(emailLabel);
		searchPanel.add(emailTextField);

		searchPanel.add(addButton);
		searchPanel.add(searchButton);
		searchPanel.add(removeButton);

		return searchPanel;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(searchButton)) {
			searchAction();
		} else if (e.getSource().equals(addButton)) {
			addAction();
		} else if (e.getSource().equals(removeButton)) {
			removeAction();
		}

	}

	private void addAction() {
		Integer matricula = Integer.parseInt(matriculaTextField.getText());
		String nome = nomeTextField.getText();
		String email = emailTextField.getText();
		new ButtonWorker(() -> {
			Aluno aluno = new Aluno(matricula, nome, email);
			searchBinaryTreePanel.add(aluno);
			searchBinaryTreePanel.repaint();
			JOptionPane.showMessageDialog(frame, "Aluno adicionado!", frame.getTitle(), JOptionPane.WARNING_MESSAGE);
		}).execute();
	}

	private void searchAction() {
		Integer matricula = Integer.parseInt(matriculaTextField.getText());
		new ButtonWorker(() -> {
			Node<Aluno> node = searchBinaryTreePanel.search(new Aluno(matricula));
			if (node == null) {
				JOptionPane.showMessageDialog(frame, "Nenhum aluno encontrado para a matrícula fornecida",
						frame.getTitle(), JOptionPane.WARNING_MESSAGE);
			} else {
				JOptionPane.showMessageDialog(frame, "Aluno encontrado=" + node.element.getMatricula() + "|"
						+ node.element.getNome() + "|" + node.element.getEmail(), frame.getTitle(),
						JOptionPane.WARNING_MESSAGE);
			}
		}).execute();
	}

	private void removeAction() {
		Integer matricula = Integer.parseInt(matriculaTextField.getText());
		new ButtonWorker(() -> {
			searchBinaryTreePanel.remove(new Aluno(matricula));
			searchBinaryTreePanel.repaint();
			JOptionPane.showMessageDialog(frame, "Aluno excluído!", frame.getTitle(), JOptionPane.WARNING_MESSAGE);
		}).execute();
	}

	private class ButtonWorker extends SwingWorker<Integer, Void> {

		private Runnable runnable;

		public ButtonWorker(Runnable runnable) {
			this.runnable = runnable;
		}

		@Override
		protected Integer doInBackground() throws Exception {
			runnable.run();
			return null;
		}
	}

}
