package br.ucsal.bes20201.ed.aula02;

public class Exemplo2 {

	private static final int QTD = 10;

	public static void main(String[] args) {

		Integer[] matriculas = new Integer[QTD];
		String[] nomes = new String[QTD];
		String[] emails = new String[QTD];

		matriculas[0] = 123;
		nomes[0] = "Claudio Neiva";
		emails[0] = "antonio.neiva@pro.ucsal.br";

		matriculas[1] = 678;
		nomes[1] = "Maria da Silva";
		emails[1] = "maria.silva@ucsal.br";

		int auxMatricula;
		String auxNome;
		String auxEmails;

		for (int i = 0; i < QTD - 1; i++) {
			for (int j = i + 1; j < QTD; j++) {
				if (matriculas[i] > matriculas[j]) {
					auxMatricula = matriculas[i];
					matriculas[i] = matriculas[j];
					matriculas[j] = auxMatricula;

					auxNome = nomes[i];
					nomes[i] = nomes[j];
					nomes[j] = auxNome;

					auxEmails = emails[i];
					emails[i] = emails[j];
					emails[j] = auxEmails;
				}
			}
		}

		exibirAlunos(matriculas, nomes, emails);

	}

	private static void exibirAlunos(Integer[] matriculas, String[] nomes, String[] emails) {
		for (int i = 0; i < QTD; i++) {
			System.out.println("Matrícula=" + matriculas[i]);
			System.out.println("Nome=" + nomes[i]);
			System.out.println("Email=" + emails[i]);
		}
	}
}
