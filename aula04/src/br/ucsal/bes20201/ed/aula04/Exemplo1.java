package br.ucsal.bes20201.ed.aula04;

import java.util.LinkedList;
import java.util.List;

public class Exemplo1 {

	public static void main(String[] args) {
		Integer a = 20;
		Integer b = 34;
		Integer c = a + b;
		System.out.println(c);

		List<String> nomes = new LinkedList<>();
		nomes.add("claudio");
		nomes.add("maria");
		nomes.add("pedro");
		System.out.println(nomes.size());
		System.out.println(nomes.contains("maria"));
		System.out.println(nomes.get(0));
		nomes.set(0, "joaquim");
		System.out.println(nomes.get(0));
		// nomes.elementData[2]="caju";
	}

}
