package br.ucsal.bes20201.ed.aula14;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculoUtilTest {

	@Test
	public void testarFatorialIterativo0() {
		verificarFatorial(new CalculoUtilIterativo(), 0, 1L);
	}

	@Test
	public void testarFatorialIterativo5() {
		verificarFatorial(new CalculoUtilIterativo(), 5, 120L);
	}

	@Test
	public void testarFatorialRecursivo0() {
		verificarFatorial(new CalculoUtilRecursivo(), 0, 1L);
	}

	@Test
	public void testarFatorialRecursivo5() {
		verificarFatorial(new CalculoUtilRecursivo(), 5, 120L);
	}

	private void verificarFatorial(CalculoUtil calculoUtil, Integer n, Long fatorialEsperado) {
		Long fatorialAtual = calculoUtil.calcularFatorial(n);
		Assertions.assertEquals(fatorialEsperado, fatorialAtual);
	}

}
