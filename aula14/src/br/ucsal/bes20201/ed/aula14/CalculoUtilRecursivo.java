package br.ucsal.bes20201.ed.aula14;

public class CalculoUtilRecursivo implements CalculoUtil {

	@Override
	public Long calcularFatorial(Integer n) {
		if (n == 0) {
			return 1L;
		} else {
			return n * calcularFatorial(n - 1);
		}
	}

	/*
	 * fatorial(n)
	 * 
	 * se n=0 -> 1
	 * 
	 * se n>0 -> n * fatorial(n-1)
	 * 
	 */

}
