package br.ucsal.bes20201.ed.aula05;

public interface ListaString {
	
	void add(String element);
	
	String get(int index);
	
	String remove(int index);
	
	int size();
	
	boolean isEmpty();
	
	void clear();

}
