package br.ucsal.bes20201.ed.ordenacao.search;

public class Search {

	public static <T extends Comparable<T>> Integer sequencialSearch(T[] data, T element)
			throws ElementNotFoundException {
		for (int i = 0; i < data.length; i++) {
			if (data[i].equals(element)) {
				return i;
			}
		}
		throw new ElementNotFoundException();
	}

	public static <T extends Comparable<T>> Integer binarySearchIterative(T[] data, T element)
			throws ElementNotFoundException {
		int start = 0;
		int end = data.length - 1;
		while (start <= end) {
			int half = (start + end) / 2;
			int comparisonResult = element.compareTo(data[half]);
			switch (comparisonResult) {
			case 0:
				return half;
			case 1:
				start = half + 1;
				break;
			case -1:
				end = half - 1;
				break;
			}
		}
		throw new ElementNotFoundException();
	}

	public static <T extends Comparable<T>> Integer binarySearchRecursive(T[] data, T element)
			throws ElementNotFoundException {
		return binarySearchRecursive(data, element, 0, data.length - 1);
	}

	private static <T extends Comparable<T>> Integer binarySearchRecursive(T[] data, T element, int start, int end)
			throws ElementNotFoundException {
		if (start > end) {
			throw new ElementNotFoundException();
		}
		int half = (start + end) / 2;
		int comparisonResult = element.compareTo(data[half]);
		switch (comparisonResult) {
		case 0:
			return half;
		case 1:
			return binarySearchRecursive(data, element, start + 1, end);
		default:
			return binarySearchRecursive(data, element, start, half - 1);
		}
	}

}
