package br.ucsal.bes20201.ed.ordenacao.search;

import java.util.Scanner;

import br.ucsal.bes20201.ed.ordenacao.sort.Sort;
import br.ucsal.bes20201.ed.ordenacao.util.DataGenerator;
import br.ucsal.bes20201.ed.ordenacao.util.TestDataTypeEnum;

public class SearchExample {

	private static final int QTY_NUMBERS = 20;

	private static final int MIN = 0;
	private static final int MAX = 100;

	private static Integer[] data = DataGenerator.generateData(QTY_NUMBERS, MIN, MAX, TestDataTypeEnum.RANDOM);

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		searchExample();
	}

	private static void searchExample() {
		sortData();
		while (true) {
			showSearchAlgorithms();
			SearchAlgorithmEnum searchAlgorithmSelected = getSelectedSearchAlgorithm();
			executeSearchAlgorithm(searchAlgorithmSelected);
		}
	}

	private static void sortData() {
		Sort.bubbleSort(data);
	}

	private static void showSearchAlgorithms() {
		for (SearchAlgorithmEnum searchAlgorithm : SearchAlgorithmEnum.values()) {
			System.out.println(searchAlgorithm.getFullDescription());
		}
	}

	private static SearchAlgorithmEnum getSelectedSearchAlgorithm() {
		while (true) {
			System.out.println("Informe o código do algoritmo desejado: ");
			try {
				return SearchAlgorithmEnum.valueOfCod(scanner.nextInt());
			} catch (Exception e) {
				System.out.println("Opção inválida!");
				// Limpa o buffer caso seja informado um texto no lugar de um número.
				if (scanner.hasNextLine()) {
					scanner.nextLine();
				}
			}
		}
	}

	private static void executeSearchAlgorithm(SearchAlgorithmEnum searchAlgorithm) {
		System.out.println("\n\n" + searchAlgorithm.getDescription());
		switch (searchAlgorithm) {
		case SEQUENCIAL_SEARCH:
			executeSearch(Search::sequencialSearch);
			break;
		case BINARY_SEARCH_ITERATIVE:
			executeSearch(Search::binarySearchIterative);
			break;
		case BINARY_SEARCH_RECURSIVE:
			executeSearch(Search::binarySearchRecursive);
			break;
		}
		System.out.println("\n\n");
	}

	private static void executeSearch(
			BiFunctionThrowElementNotFoundException<Integer[], Integer, Integer> searchAlgorithm) {
		Sort.show("Dados: ", data);
		System.out.println("Informe o número a ser pesquisado:");
		Integer element = scanner.nextInt();
		try {
			Integer pos = searchAlgorithm.apply(data, element);
			System.out.println("Elemento encontrado na posição " + pos + ".");
		} catch (ElementNotFoundException e) {
			System.out.println("Elemento não encontrado.");
		}
	}

}
