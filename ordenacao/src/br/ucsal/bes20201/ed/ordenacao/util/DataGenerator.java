package br.ucsal.bes20201.ed.ordenacao.util;

public class DataGenerator {
	
	public static Integer[] generateData(int qty, int min, int max, TestDataTypeEnum type) {
		Integer[] dataRandom = new Integer[qty];
		for (int i = 0; i < qty; i++) {
			switch (type) {
			case RANDOM:
				dataRandom[i] = (int) Math.round(Math.random() * (max - min + 1) + min);
				break;
			case BEST:
				dataRandom[i] = min + i;
				break;
			case WORSE:
				dataRandom[i] = max - i;
				break;
			default:
				break;
			}
		}
		return dataRandom;
	}

}
