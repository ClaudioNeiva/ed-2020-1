package br.ucsal.bes20201.ed.ordenacao.sort;

import java.util.Scanner;
import java.util.function.Consumer;

import br.ucsal.bes20201.ed.ordenacao.util.DataGenerator;
import br.ucsal.bes20201.ed.ordenacao.util.TestDataTypeEnum;

public class SortExample {

	private static final int QTY_NUMBERS = 8;

	private static final int MIN = 0;
	private static final int MAX = 100;

	private static Integer[] dataBestCase = DataGenerator.generateData(QTY_NUMBERS, MIN, MAX, TestDataTypeEnum.BEST);
	private static Integer[] dataWorseCase = DataGenerator.generateData(QTY_NUMBERS, MIN, MAX, TestDataTypeEnum.WORSE);
	private static Integer[] dataRandom = DataGenerator.generateData(QTY_NUMBERS, MIN, MAX, TestDataTypeEnum.RANDOM);

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		sortExample();
	}

	private static void sortExample() {
		while (true) {
			showSortAlgorithms();
			SortAlgorithmEnum sortAlgorithmSelected = getSelectedSortAlgorithm();
			executeSelectedSortAlgorithm(sortAlgorithmSelected);
		}
	}

	private static void showSortAlgorithms() {
		for (SortAlgorithmEnum sortAlgorithm : SortAlgorithmEnum.values()) {
			System.out.println(sortAlgorithm.getFullDescription());
		}
	}

	private static SortAlgorithmEnum getSelectedSortAlgorithm() {
		while (true) {
			System.out.println("Informe o código do algoritmo desejado: ");
			try {
				return SortAlgorithmEnum.valueOfCod(scanner.nextInt());
			} catch (Exception e) {
				System.out.println("Opção inválida!");
				// Limpa o buffer caso seja informado um texto no lugar de um número.
				if (scanner.hasNextLine()) {
					scanner.nextLine();
				}
			}
		}
	}

	private static void executeSelectedSortAlgorithm(SortAlgorithmEnum sortAlgorithm) {
		System.out.println("\n\n" + sortAlgorithm.getDescription());
		switch (sortAlgorithm) {
		case SELECTION_SORT:
			executeSort(Sort::selectionSort);
			break;
		case BUBBLE_SORT:
			executeSort(Sort::bubbleSort);
			break;
		case INSERTION_SORT:
			executeSort(Sort::insertionSort);
			break;
		case MERGE_SORT:
			executeSort(Sort::mergeSort);
			break;
		}
		System.out.println("\n\n");
	}

	private static void executeSort(Consumer<Integer[]> sortAlgorithm) {
		executeSort("Melhor caso", dataBestCase, sortAlgorithm);
		executeSort("Pior caso", dataWorseCase, sortAlgorithm);
		executeSort("Dados aleatórios", dataRandom, sortAlgorithm);
	}

	private static void executeSort(String title, Integer[] data, Consumer<Integer[]> sortAlgorithm) {
		System.out.println("\n*************" + title + "*************");
		Sort.show("Dados originais:", data);
		sortAlgorithm.accept(data);
		Sort.show("Dados ordenados:", data);
	}

}
