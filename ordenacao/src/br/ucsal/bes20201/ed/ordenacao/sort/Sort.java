package br.ucsal.bes20201.ed.ordenacao.sort;

/**
 * 
 * https://www.geeksforgeeks.org/sorting-algorithms/
 *
 */
public class Sort {

	private static final boolean DEBUG = false;

	/**
	 * Utilitário para exibição de dados.
	 */
	public static <T extends Comparable<T>> void show(String message, T[] data) {
		if (message != null) {
			System.out.println(message);
		}
		for (int i = 0; i < data.length; i++) {
			System.out.print(data[i] + " \t");
		}
		System.out.println();
	}

	/**
	 * 
	 * Passar o menor valor para a primeira posição, segundo menor valor para
	 * segunda posição, e assim sucessivamente.
	 * 
	 * complexidade pior caso O(n^2);
	 * 
	 * complexidade caso médio O(n^2)
	 *
	 * complexidade melhor caso O(n^2)
	 * 
	 * https://pt.wikipedia.org/wiki/Selection_sort
	 */
	public static <T extends Comparable<T>> void selectionSort(T[] data) {
		for (int i = 0; i < data.length - 1; i++) {
			for (int j = i + 1; j < data.length; j++) {
				if (data[i].compareTo(data[j]) > 0) {
					swap(data, i, j);
				}
			}
			if (DEBUG) {
				show("i=" + i, data);
			}
		}
	}

	/**
	 * 
	 * Percorrer o vetor diversas vezes, deslocando o maior elemento para o final,
	 * até que não nenhum elemento seja deslocado (vetor ordenado).
	 * 
	 * complexidade pior caso O(n^2);
	 * 
	 * complexidade caso médio O(n^2)
	 *
	 * complexidade melhor caso O(n)
	 * 
	 * https://pt.wikipedia.org/wiki/Bubble_sort
	 */
	public static <T extends Comparable<T>> void bubbleSort(T[] data) {
		Boolean change;
		int qty = data.length;
		do {
			qty--;
			change = false;
			for (int i = 0; i < qty; i++) {
				if (data[i].compareTo(data[i + 1]) > 0) {
					swap(data, i, i + 1);
					change = true;
				}
			}
		} while (change);
	}

	/**
	 * 
	 * Posicionar um elemento em relação aos anteriores, até posicionar a último.
	 * Como se fosse um jogador de baralho ordenando as cartas.
	 * 
	 * complexidade pior caso O(n^2);
	 * 
	 * complexidade caso médio O(n^2)
	 *
	 * complexidade melhor caso O(n)
	 * 
	 * https://pt.wikipedia.org/wiki/Insertion_sort
	 */
	public static <T extends Comparable<T>> void insertionSort(T[] data) {
		T key;
		int j;
		for (int i = 1; i < data.length; i++) {
			key = data[i];
			j = i - 1;
			while (j >= 0 && data[j].compareTo(key) > 0) {
				data[j + 1] = data[j];
				j = j - 1;
			}
			data[j + 1] = key;
		}
	}

	/**
	 * 
	 * Dividir o vetor em subconjuntos, ordenar e unir o resultado.
	 * 
	 * complexidade pior caso n log n;
	 * 
	 * complexidade caso médio n log n
	 *
	 * complexidade melhor caso n log n - típico
	 * 
	 * complexidade melhor caso n - variante natural
	 * 
	 * https://pt.wikipedia.org/wiki/Merge_sort
	 */
	public static <T extends Comparable<T>> void mergeSort(T[] data) {
		mergeSort(data, 0, data.length - 1);
	}

	/**
	 * Fonte: https://pt.wikipedia.org/wiki/Merge_sort
	 */
	private static <T extends Comparable<T>> void mergeSort(T[] data, int start, int end) {
		if (start < end) {
			int half = ((end + start) / 2);

			mergeSort(data, start, half);

			mergeSort(data, half + 1, end);

			merge(data, start, half, end);
		}
	}

	/**
	 * Fonte: https://pt.wikipedia.org/wiki/Merge_sort
	 */
	@SuppressWarnings("unchecked")
	public static <T extends Comparable<T>> void merge(T[] data, int start, int half, int end) {

		T[] tmp = (T[]) new Comparable[data.length];

		for (int i = start; i <= end; i++) {
			tmp[i] = data[i];
		}

		int i = start;
		int j = half + 1;
		int k = start;

		while (i <= half && j <= end) {
			if (tmp[i].compareTo(tmp[j]) < 0) {
				data[k] = tmp[i];
				i++;
			} else {
				data[k] = tmp[j];
				j++;
			}
			k++;
		}

		while (i <= half) {
			data[k] = tmp[i];
			i++;
			k++;
		}

		while (j <= end) {
			data[k] = tmp[j];
			j++;
			k++;
		}
	}

	private static <T> void swap(T[] data, int i, int j) {
		T aux = data[i];
		data[i] = data[j];
		data[j] = aux;
	}

}
