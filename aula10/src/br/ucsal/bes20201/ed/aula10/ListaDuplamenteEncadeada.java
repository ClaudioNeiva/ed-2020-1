package br.ucsal.bes20201.ed.aula10;

public class ListaDuplamenteEncadeada<T> implements Lista<T> {

	private No cabeca;

	private No cauda;

	private int qtd;

	public ListaDuplamenteEncadeada() {
		cabeca = new No();
		cauda = new No();
		cabeca.prox = cauda;
		cauda.ant = cabeca;
		qtd = 0;
	}

	@Override
	public void add(T element) {
		No novo = new No(element);

		novo.ant = cauda.ant;
		novo.prox = cauda;

		cauda.ant.prox = novo;
		cauda.ant = novo;

		qtd++;
	}

	@Override
	public T get(int index) {
		No aux = cabeca.prox;
		int i = 0;
		while (aux != cauda && i < index) {
			aux = aux.prox;
			i++;
		}
		if (aux == cauda) {
			return null;
		}
		return aux.element;
	}

	@Override
	public T remove(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int size() {
		return qtd;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub

	}

	private class No {
		private T element;
		private No prox;
		private No ant;

		private No() {
		}

		private No(T element) {
			this.element = element;
		}

	}

}
