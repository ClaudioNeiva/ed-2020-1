package br.ucsal.bes20201.ed.aula10;

public class ListaEncadeadaCircular<T> implements Lista<T> {

	private No cabeca;

	private int qtd = 0;

	public ListaEncadeadaCircular() {
		cabeca = new No();
		cabeca.prox = cabeca;
	}

	@Override
	public void add(T element) {
		No aux = cabeca.prox;
		while (aux.prox != cabeca) {
			aux = aux.prox;
		}

		// Aqui o aux aponta para o último elemento da lista.
		No novo = new No(element);
		novo.prox = aux.prox; // poderia ser novo.prox = cabeca;
		aux.prox = novo;
		qtd++;
	}

	@Override
	public T get(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public T remove(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int size() {
		return qtd;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
	}

	private class No {
		private T element;
		private No prox;

		private No() {
		}

		private No(T element) {
			this.element = element;
		}

	}

}
