package br.ucsal.bes20201.ed.aula10;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ListaDuplamenteEncadeadaTest {

	@Test
	public void testarGet1Elemento() {
		Lista<Integer> lista = new ListaDuplamenteEncadeada<>();
		lista.add(45);
		Assertions.assertEquals(45, lista.get(0));
	}

	@Test
	public void testarGet3Elementos() {
		Lista<Integer> lista = new ListaDuplamenteEncadeada<>();
		lista.add(45);
		lista.add(30);
		lista.add(67);
		Assertions.assertEquals(45, lista.get(0));
		Assertions.assertEquals(30, lista.get(1));
		Assertions.assertEquals(67, lista.get(2));
		Assertions.assertNull(lista.get(3));
	}

	@Test
	public void testarRemoverUltimoElemento() {
		Lista<Integer> lista = criarListaCom3Elementos();
		lista.remove(2);
		Assertions.assertEquals(45, lista.get(0));
		Assertions.assertEquals(30, lista.get(1));
		Assertions.assertNull(lista.get(2));
	}

	@Test
	public void testarRemoverPrimeiroElemento() {
		Lista<Integer> lista = criarListaCom3Elementos();
		lista.remove(0);
		Assertions.assertEquals(30, lista.get(0));
		Assertions.assertEquals(67, lista.get(1));
		Assertions.assertNull(lista.get(2));
	}

	@Test
	public void testarTamanho() {
		Lista<Integer> lista = criarListaCom3Elementos();
		Assertions.assertEquals(3, lista.size());
	}

	@Test
	public void testarTamanhoAposRemocao() {
		Lista<Integer> lista = criarListaCom3Elementos();
		lista.remove(0);
		Assertions.assertEquals(2, lista.size());
	}

	@Test
	public void testarListaNaoVazia() {
		Lista<Integer> lista = criarListaCom3Elementos();
		Assertions.assertFalse(lista.isEmpty());
	}

	@Test
	public void testarListaVazia() {
		Lista<Integer> lista = new ListaDuplamenteEncadeada<>();
		Assertions.assertTrue(lista.isEmpty());
	}

	@Test
	public void testarLimparLista() {
		Lista<Integer> lista = criarListaCom3Elementos();
		lista.clear();
		Assertions.assertTrue(lista.isEmpty());
	}

	private Lista<Integer> criarListaCom3Elementos() {
		Lista<Integer> lista = new ListaDuplamenteEncadeada<>();
		lista.add(45);
		lista.add(30);
		lista.add(67);
		return lista;
	}

	/*
	 * Para que quiser se aprofundar em JUnit, estudar a anotação @BeforeEach
	 * (JUnit5) ou @Before (JUnit4). A criação da lista e adição dos elementos 45,
	 * 30 e 67 poderia ficar num método de setup.
	 */

}
