package br.ucsal.bes20201.ed.aula13;

public class ArrayQueue<T> implements Queue<T> {

	private static final int MAX = 5;

	private Object[] objects = new Object[MAX];

	/*
	 * Não é necessário inicializar com zero, pois os ATRIBUTOS do tipo int já são
	 * inicializados com zero por default;
	 */
	private int start = 0;

	private int end = 0;

	private int size = 0;

	@Override
	public Boolean add(T element) {
		if(size == MAX) {
			throw new RuntimeException("Queue full.");
		}
		objects[end] = element;
		end++;
		if (end == MAX) {
			end = 0;
		}
		size++;
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public T remove() {
		if(isEmpty()) {
			throw new RuntimeException("Queue empty.");
		}
		T element = (T) objects[start];
		objects[start] = null;
		start++;
		if (start == MAX) {
			start = 0;
		}
		size--;
		return element;
	}

	@SuppressWarnings("unchecked")
	@Override
	public T element() {
		if(isEmpty()) {
			throw new RuntimeException("Queue empty.");
		}
		return (T) objects[start];
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public int size() {
		return size;
	}

}
