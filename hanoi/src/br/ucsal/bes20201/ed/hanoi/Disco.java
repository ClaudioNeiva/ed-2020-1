package br.ucsal.bes20201.ed.hanoi;

import java.awt.Color;

public class Disco {

	public static final int ALTURA = 20;

	public static final int ARCO_BORDA = 10;

	private int diametro;
	
	private Color cor;

	public Disco(int diametro, Color cor) {
		super();
		this.diametro = diametro;
		this.cor = cor;
	}

	public int getDiametro() {
		return diametro;
	}

	public Color getCor() {
		return cor;
	}

}
