package br.ucsal.bes20201.ed.hanoi;

public class Comando {

	private int qtd;

	private Stack<Disco> torreOrigem;

	private Stack<Disco> torreDestino;

	private Stack<Disco> torreTrabalho;

	public Comando(int qtd, Stack<Disco> torreOrigem, Stack<Disco> torreDestino, Stack<Disco> torreTrabalho) {
		super();
		this.qtd = qtd;
		this.torreOrigem = torreOrigem;
		this.torreDestino = torreDestino;
		this.torreTrabalho = torreTrabalho;
	}

	public int getQtd() {
		return qtd;
	}

	public Stack<Disco> getTorreOrigem() {
		return torreOrigem;
	}

	public Stack<Disco> getTorreDestino() {
		return torreDestino;
	}

	public Stack<Disco> getTorreTrabalho() {
		return torreTrabalho;
	}

}
