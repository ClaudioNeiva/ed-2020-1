package br.ucsal.bes20201.ed.aula08.exemplo1;

import java.util.Scanner;

public class Exemplo1ListaContinua {

	private static final int QTD = 50000;

	private static Scanner sc = new Scanner(System.in);
	private static Aluno[] alunos = new Aluno[QTD];
	private static int qtd = 0;

	public static void main(String[] args) {
		obterAlunos();
		exibirAlunos();
	}

	private static void exibirAlunos() {
		System.out.println("Alunos cadastrados:");
		for (int i = 0; i < qtd; i++) {
			System.out.println(alunos[i]);
		}
	}

	private static void obterAlunos() {
		String resposta;
		Integer matricula;
		String nome;

		do {
			System.out.println("Informe os dados dos alunos:");
			System.out.println("Matrícula:");
			matricula = sc.nextInt();
			sc.nextLine();
			System.out.println("Nome:");
			nome = sc.nextLine();

			alunos[qtd] = new Aluno(matricula, nome);
			qtd++;

			System.out.println("Deseja informar mais um aluno? (S/N):");
			resposta = sc.nextLine();
		} while (resposta.equalsIgnoreCase("S"));

	}

}
