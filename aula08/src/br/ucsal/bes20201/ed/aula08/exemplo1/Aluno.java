package br.ucsal.bes20201.ed.aula08.exemplo1;

public class Aluno {

	Integer matricula;

	String nome;

	// FIXME Refatorar para separar a estrutura encadeada da estrutura de aluno.
	Aluno prox;
	
	public Aluno(Integer matricula, String nome) {
		super();
		this.matricula = matricula;
		this.nome = nome;
	}

	@Override
	public String toString() {
		return "Aluno [matricula=" + matricula + ", nome=" + nome + "]";
	}

}
