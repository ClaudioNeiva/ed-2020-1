package br.ucsal.bes20201.ed.aula08.listasimplesmenteencadeada;

public interface ListaAluno {
	
	void add(Aluno element);
	
	Aluno get(int index);
	
	Aluno remove(int index);
	
	int size();
	
	boolean isEmpty();
	
	void clear();

}
