package br.ucsal.bes20201.ed.atividade4;

public class EncadeadaListaInteger implements ListaInteger {

	private No inicio;

	private int size = 0;

	@Override
	public void add(Integer element) {
		No novo = new No(element);
		if (inicio == null) {
			inicio = novo;
		} else {
			No aux = inicio;
			while (aux.prox != null) {
				aux = aux.prox;
			}
			aux.prox = novo;
		}
		size++;
	}

	@Override
	public Integer get(int index) {
		No aux = inicio;
		int i = 0;
		while (aux != null && i < index) {
			aux = aux.prox;
			i++;
		}
		if (aux == null) {
			return null;
		}
		return aux.element;
	}

	@Override
	public void addAll(ListaInteger list) {
		int i = 0;
		while (list.get(i) != null) {
			add(list.get(i));
			i++;
		}
	}

	public void addAllComSize(ListaInteger list) {
		for (int i = 0; i < list.size(); i++) {
			add(list.get(i));
		}
	}

	public int size() {
		return size;
	}

	private class No {
		private Integer element;
		private No prox;

		private No(Integer element) {
			this.element = element;
		}
	}

}
